// Example to process CSV data
// Table syntax in Processing reference: <a href="http://www.processing.org/reference/Table.html" target="_blank">http://www.processing.org/reference/Table.html</a>
// copyleft: kasperkamperman.com 17-05-2014

String filename = "articles2";
Table table;
String neutral = "fact";
String right = "right";
String left = "left";

StringBuffer factBuffer = new StringBuffer();
StringBuffer rightBuffer = new StringBuffer();
StringBuffer leftBuffer = new StringBuffer();

String[] news = {"Atlantic", 
  "Breitbart", 
  "Business Insider", 
  "Buzzfeed News", 
  "CNN", 
  "Fox News", 
  "Guardian", 
  "National Review", 
  "New York Times", 
  "NPR", 
  "Reuters", 
  "Talking Points Memo", 
  "Vox", 
  "Washington Post" };

String[] labels = {"left", 
  "right", 
  "left", 
  "left", 
  "fact", 
  "right", 
  "fact", 
  "right", 
  "fact", 
  "fact", 
  "fact", 
  "left", 
  "links", 
  "fact"};

String[] facts    = {"CNN", "Guardian", "New York Times", "NPR", "Reuters", "Washington Post"};
String[] rights   = {"Breitbart", "Fox News", "National Review"};
String[] lefts    = {"Atlantic", "Business Insider", "Buzzfeed News", "Talking Points Memo"};


void setup() {
  loadTable();
  addCat();
  saveCatFiles();
}


void loadTable() {
  table = loadTable(filename+".csv");
  println("loaded " + filename + ".csv");
  println(table.getRowCount() + " total rows in table");
}


void saveTable() {
  saveTable(table, "data/"+filename+"_new.csv");
  println("saved "+filename+"_new.csv");
  println(table.getRowCount() + " total rows in table"); 

  println("done");
}


void test() {
  //table.addColumn("category");
  for (TableRow row : table.rows()) 
  {
    String publication = row.getString(3);

    boolean isNews = false;
    for (int i = 0; i < news.length; i++) {
      if (news[i].equals(publication)) {
        isNews = true;
        break;
      }
    }
    if (!isNews) {
      System.out.println(publication);
    }
  }
}

void addCat() {

  for (TableRow row : table.rows()) {
    String publication = row.getString(3);

    boolean wasFound = false;

    for (int i = 0; i < facts.length; i++) {
      if (facts[i].equals(publication)) {
        factBuffer.append(" ");
        factBuffer.append(row.getString(2));
        wasFound = true;
        break;
      }
    }

    if (!wasFound) {
      for (int i = 0; i < rights.length; i++) {
        if (rights[i].equals(publication)) {
          factBuffer.append(" ");
          factBuffer.append(row.getString(2));
          wasFound = true;
          break;
        }
      }
    }

    if (!wasFound) {
      for (int i = 0; i < lefts.length; i++) {
        if (lefts[i].equals(publication)) {
          factBuffer.append(" ");
          factBuffer.append(row.getString(2));
          wasFound = true;
          break;
        }
      }
    }
  }
}


void saveCatFiles() {

  try {
    PrintWriter outF = new PrintWriter("facts.txt");
    outF.println(factBuffer.toString());
    outF.flush();
    outF.close();

    PrintWriter outR = new PrintWriter("rights.txt");
    outR.println(rightBuffer.toString());
    outR.flush();
    outR.close();

    PrintWriter outL = new PrintWriter("lefts.txt");
    outL.println(leftBuffer.toString());
    outL.flush();
    outL.close();
  }
  catch(IOException e) {
    System.out.println("error");
  }
  
  System.out.println("files written");
}


void process() {

  // variables used in if-function to modify a column based on time
  int startWaitingTime = 32000;
  int stopWaitingTime  = 610000; 

  int isWaiting = 1;
  int isNotWaiting = 0;

  int timeInMillis; // timeInMillis from column
  int rowId;        // rowId from column

  for (TableRow row : table.rows()) 
  {
    //set the isWaiting flag (0,1) b
    //.getInt(row, column)
    timeInMillis = row.getInt(5);
    rowId        = row.getInt(0);

    //set the waiting flag based on timeInMillis
    if (timeInMillis>startWaitingTime && timeInMillis<stopWaitingTime) 
    { //.setInt(row, column, value)
      table.setInt(rowId, 8, isWaiting);
    } else
    { 
      table.setInt(rowId, 8, isNotWaiting);
    }
  }
}
